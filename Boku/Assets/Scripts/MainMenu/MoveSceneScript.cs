﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MoveSceneScript : MonoBehaviour
{
    public GameObject dataHandler;

    private void Start()
    {
        GetComponent<UserSelectScript>();
        dataHandler = GameObject.FindGameObjectWithTag("DataHandler");
    }

    public void ClickToStart()
    {
        SceneManager.LoadScene("AuthScene");
    }

    public void MoveToMainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void MoveToSetting()
    {
        SceneManager.LoadScene("SettingScene");
    }

    public void MoveToCredit()
    {
        SceneManager.LoadScene("CreditScene");
    }

    public void MoveLevel1()
    {
        SceneManager.LoadScene("Level1");
    }

    public void LoadGame()
    {
        dataHandler.GetComponent<UserSelectScript>().LoadGame();
        Debug.Log(UserSelectScript._progress);
        SceneManager.LoadScene("LevelSelection");
    }

    public void NewGame()
    {
        UserSelectScript.levelCleared = new bool[7];
        UserSelectScript.levelCleared[0] = true;
        UserSelectScript.starAquired = new int[7];
        UserSelectScript.score = 0;
        UserSelectScript.star = 0;
        SceneManager.LoadScene("LevelSelection");
    }

    public void closeGame()
    {
        Application.Quit();
    }
}
