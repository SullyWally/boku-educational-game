﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuHandler : MonoBehaviour
{
    public GameObject loadMenu;
    void Start()
    {
        GetComponent<UserSelectScript>();
    }

    // Update is called once per frame
    void Update()
    {
        if(UserSelectScript._progress == "1")
        {
            loadMenu.GetComponent<Button>().interactable = false;
        }
        else
        {
            loadMenu.GetComponent<Button>().interactable = true;
        }
    }
}
