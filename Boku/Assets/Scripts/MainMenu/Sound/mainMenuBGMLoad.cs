﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mainMenuBGMLoad : MonoBehaviour
{
    public static mainMenuBGMLoad mainMenuBGM;
    private void Awake() {
        if(mainMenuBGM == null){
            mainMenuBGM = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else if(mainMenuBGM != this){
            Destroy(gameObject);
        }
    }
}
