﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingScript : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject loadingScreen;
    public Slider slider;
    public Text progressText;
    public string scene;
    void Start()
    {
        
    }

    public void LoadScene()
    {
        StartCoroutine(LoadAsyncScene(scene));
    }

    public void LoadLevel1()
    {
        StartCoroutine(LoadAsyncScene("Level1"));
    }

    public void LoadLevel2()
    {
        StartCoroutine(LoadAsyncScene("Level2"));
    }

    public void LoadLevel3()
    {
        StartCoroutine(LoadAsyncScene("Level3"));
    }

    public void LoadLevel4()
    {
        StartCoroutine(LoadAsyncScene("Level4"));
    }

    public void LoadLevel5()
    {
        StartCoroutine(LoadAsyncScene("Level5"));
    }

    public void LoadLevel6()
    {
        StartCoroutine(LoadAsyncScene("Level6"));
    }

    IEnumerator LoadAsyncScene(string sceneName)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneName);
        loadingScreen.SetActive(true);

        while(!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / .9f);

            slider.value = progress;
            progressText.text = progress * 100f + "%";

            yield return null;
        }
    }
}
