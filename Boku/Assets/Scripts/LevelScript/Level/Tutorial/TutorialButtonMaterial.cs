﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialButtonMaterial : MonoBehaviour
{
    public InventorySystem roboInventory;

    public GameObject NextButton;

    public int TargetBesi;
    public int TargetKaca;
    public int TargetKaret;
    public int TargetPlastik;


    void Start(){
        NextButton.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(roboInventory.jumlahKaret >= TargetKaret && roboInventory.jumlahBesi >= TargetBesi && roboInventory.jumlahKaca >= TargetKaca && roboInventory.jumlahPlastik >= TargetPlastik){
            NextButton.SetActive(true);
        }
    }
}
