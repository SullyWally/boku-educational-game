﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Level2Script : MonoBehaviour
{
    public int star;

    public GridBehavior level;
    public InventorySystem playerInventory;

    public GameObject UIAction;
    public GameObject UITask;
    public GameObject UIInventory;
    public GameObject UIToggleButton;
    public GameObject WinScreen;

    public GameObject [] starUI;

    public PauseHandlerScript timer;

    public int score;
    public Text scoreText;

    private bool levelComplete;
    private int starRemoved;

    void Start()
    {
        levelComplete = false;
        starRemoved = 3;
        GetComponent<UserSelectScript>();
    }

    public void level2Cleared()
    {
        if(UserSelectScript.levelCleared[2] == false){
            UserSelectScript.levelComplete += 1;
            UserSelectScript.levelCleared[2] = true;
        }
        if(UserSelectScript.starAquired[1] < star){
            UserSelectScript.star += star - UserSelectScript.starAquired[1];
            UserSelectScript.starAquired[1] = star;
            UserSelectScript.score += score;
        }
        SceneManager.LoadScene("LevelSelection");
    }

    void Update()
    {
        if(!levelComplete){
            if(TaskFulfilled()){
                levelComplete = true;
                level.moving = false;
                StartCoroutine(winScreenAnimation());
            }
        }
    }

    IEnumerator winScreenAnimation()
    {
        yield return new WaitForSeconds(1f);
        UIAction.SetActive(false);
        UITask.SetActive(false);
        UIInventory.SetActive(false);
        UIToggleButton.SetActive(false);

        WinScreen.SetActive(true);
        WinScreen.GetComponent<Animator>().SetTrigger("view");
        yield return new WaitForSeconds(0.5f);        
        while (starRemoved > star)
        {
            starRemoved--;
            starUI[starRemoved].SetActive(false);
        }
    }

    bool TaskFulfilled(){
        if(playerInventory.jumlahMesin >= 1 && playerInventory.jumlahBan >= 1 && playerInventory.jumlahKacaMob >= 1){
            star++;
            score = 1000 - Mathf.FloorToInt(timer.timeRemaining) - level.actionExecuted * 9;
            if (score < 0){
                score = 0;
            }
            scoreText.text = score.ToString();

            if(score >= 675){
                star++;
            }
            if(score >= 750){
                star++;
            }
            return true;
        }
        else
        {
            return false;
        }
    }
}
