﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleCommandButton : MonoBehaviour
{
    public GameObject commandButton;
    public GameObject dropDownButton;

    public Transform buttonTargetPosition;

    private bool dropDown;
    public GameObject originalPos;
    public Transform dropDownOriginalPos;


    private GameObject [] allButton;

    public void Start() {
        dropDown = false;
        
        allButton = GameObject.FindGameObjectsWithTag("CommandButton");
    }   

    public void ToggleButton(){

        switch (commandButton.name){
            case "MoveUIButton" :
                dropDownButton = GameObject.Find("AllMoveButton");
                break;
            case "BeliUIButton" :
                dropDownButton = GameObject.Find("AllBeliButton");
                break;
            case "BuatUIButton" :
                dropDownButton = GameObject.Find("AllBuatButton");
                break;
            case "IfUIButton" :
                dropDownButton = GameObject.Find("AllIfButton");
                break;
            case "LoopUIButton" :
                dropDownButton = GameObject.Find("AllLoopButton");
                break;
            default :
                break;
        }

        if(!dropDown)
            DropDownCommandButton();
        else
            ReturnCommandButton();
    }



    public void DropDownCommandButton(){
        foreach(GameObject comButton in allButton){
            comButton.SetActive(false);
        }
        commandButton.SetActive(true);
        originalPos.GetComponent<RectTransform>().anchoredPosition = commandButton.GetComponent<RectTransform>().anchoredPosition;
        commandButton.transform.position = buttonTargetPosition.transform.position;
        
        dropDownButton.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
        dropDown = true;
    }

    public void ReturnCommandButton(){
        
        commandButton.GetComponent<RectTransform>().anchoredPosition = originalPos.GetComponent<RectTransform>().anchoredPosition;
        dropDownButton.transform.position = dropDownOriginalPos.position;
        
        foreach(GameObject comButton in allButton){
            comButton.SetActive(true);
        }
        dropDown = false;
    }
}
