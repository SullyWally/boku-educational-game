﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseHandlerScript : MonoBehaviour
{
    //PAUSE
    public GameObject pauseScreen;
    public GameObject pauseBox;

    //TIMER
    public float timeRemaining = 0;
    public bool running = false;
    public Text timeText;

    void Start()
    {
        running = true;
    }

    void Update()
    {
        if (running)
        {            
            timeRemaining += Time.deltaTime;
            DisplayTime(timeRemaining);            
        }
    }

    void DisplayTime(float timeToDisplay)
    {
        timeToDisplay += 1;

        float minutes = Mathf.FloorToInt(timeToDisplay / 60);
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);

        timeText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }

    public void PauseGame()
    {
        running = false;
        pauseScreen.SetActive(true);
        pauseBox.SetActive(true);
        pauseBox.GetComponent<Animator>().SetTrigger("view");
    }

    public void ResumeGame()
    {
        running = true;
        StartCoroutine(popInAnim());      
    }

    IEnumerator popInAnim()
    {
        pauseBox.GetComponent<Animator>().SetTrigger("hidden");
        yield return new WaitForSeconds(0.5f);
        pauseScreen.SetActive(false);
        pauseBox.SetActive(false);
    }

    public void BacktoMenu()
    {
        SceneManager.LoadScene("LevelSelection");
    }
}
