﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConditionTab : MonoBehaviour
{
    public GameObject conditionTab;
    public GridBehavior grid;

    public bool ifConditionFulfilled;

    private bool holdObject;
    private string objectCurrentlyHold;

    public InventorySystem playerInventory;
    // Start is called before the first frame update
    public void Start()
    {
        ifConditionFulfilled = true;
        holdObject = false;
        objectCurrentlyHold = "";
    }

    public void openConditionTab() {
        conditionTab.SetActive(true);
        conditionTab.GetComponent<Animator>().SetTrigger("view");
    }

    public void closeConditionTab(){
        StartCoroutine(popIn());
    }

    IEnumerator popIn()
    {
        conditionTab.GetComponent<Animator>().SetTrigger("hidden");
        yield return new WaitForSeconds(0.8f);
        conditionTab.SetActive(false);
    }

    public void ifBesiTrue(){
        if(playerInventory.jumlahBesi > 0){
            ifConditionFulfilled = true;
            grid.ifConditionMet = ifConditionFulfilled;
        }
        else{
            ifConditionFulfilled = false;
            grid.ifConditionMet = ifConditionFulfilled;
        }
        grid.index++;
    }

    public void ifKacaTrue(){
        if (playerInventory.jumlahKaca > 0){
            ifConditionFulfilled = true;
            grid.ifConditionMet = ifConditionFulfilled;
        }
        else
        {
            ifConditionFulfilled = false;
            grid.ifConditionMet = ifConditionFulfilled;
        }
        grid.index++;
    }

    public void ifKaretTrue(){
        if (playerInventory.jumlahKaret > 0){
            ifConditionFulfilled = true;
            grid.ifConditionMet = ifConditionFulfilled;
        }
        else
        {
            ifConditionFulfilled = false;
            grid.ifConditionMet = ifConditionFulfilled;
        }
        grid.index++;
    }

    public void ifPlastikTrue(){
        if (playerInventory.jumlahPlastik > 0){
            ifConditionFulfilled = true;
            grid.ifConditionMet = ifConditionFulfilled;
        }
        else
        {
            ifConditionFulfilled = false;
            grid.ifConditionMet = ifConditionFulfilled;
        }
        grid.index++;
    }

    public void ifUangTrue(){
        if (playerInventory.jumlahUang > 100){
            ifConditionFulfilled = true;
            grid.ifConditionMet = ifConditionFulfilled;
        }
        else
        {
            ifConditionFulfilled = false;
            grid.ifConditionMet = ifConditionFulfilled;
        }
        grid.index++;
    }


}
