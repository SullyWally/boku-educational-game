﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StationsScript : MonoBehaviour
{
    public enum StationList
    {
        Mesin, 
        Body,
        Ban,
        Kaca,
        Kerangka,
        Penggabungan,
    }
    public StationList Station = StationList.Mesin;

    public int Xpos;
    public int Ypos;
}
