﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class InventorySystem : MonoBehaviour
{
    public GameObject canvas;
    public GameObject[] listSparepart;

    [Header("RESOURCE")]
    public GameObject uang;
    public GameObject besi;
    public GameObject kaca;
    public GameObject karet;
    public GameObject plastik;

    [Header("JUMLAH RESOURCES")]
    public int jumlahBesi;
    public int jumlahKaca;
    public int jumlahKaret;
    public int jumlahPlastik;
    public int jumlahUang;    

    [Header("SPARE PART")]
    public GameObject mesin;
    public GameObject body;
    public GameObject kerangka;
    public GameObject ban;
    public GameObject kacaMobil;
    public GameObject Mobil;

    [Header("JUMLAH SPARE PART")]
    public int jumlahMesin;
    public int jumlahBody;
    public int jumlahKerangka;
    public int jumlahBan;
    public int jumlahKacaMob;
    public int jumlahMobil;

    int index;

    public GameObject storageZero;
    void Start()
    {
        index = 0;
        listSparepart = new GameObject[20];
    }

    //RESOURCE
    public void simpanBesi()
    {
        StartCoroutine(delayAnim());
        IEnumerator delayAnim()
        {
            uang.GetComponent<Animator>().SetTrigger("Dec");
            besi.GetComponent<Animator>().SetTrigger("Inc");
            yield return new WaitForSeconds(0.4f);
            jumlahUang -= 100;
            jumlahBesi += 1;
        }        
    }

    public void simpanKaca()
    {
        StartCoroutine(delayAnim());
        IEnumerator delayAnim()
        {
            uang.GetComponent<Animator>().SetTrigger("Dec");
            kaca.GetComponent<Animator>().SetTrigger("Inc");
            yield return new WaitForSeconds(0.4f);
            jumlahUang -= 100;
            jumlahKaca += 1;
        }
    }

    public void simpanKaret()
    {
        StartCoroutine(delayAnim());
        IEnumerator delayAnim()
        {
            uang.GetComponent<Animator>().SetTrigger("Dec");
            karet.GetComponent<Animator>().SetTrigger("Inc");
            yield return new WaitForSeconds(0.4f);
            jumlahUang -= 100;
            jumlahKaret += 1;
        }
    }

    public void simpanPlastik()
    {
        StartCoroutine(delayAnim());
        IEnumerator delayAnim()
        {
            uang.GetComponent<Animator>().SetTrigger("Dec");
            plastik.GetComponent<Animator>().SetTrigger("Inc");
            yield return new WaitForSeconds(0.4f);
            jumlahUang -= 100;
            jumlahPlastik += 1;
        }
    }

    //PRODUCT
    public void simpanKacaMob()
    {
        jumlahKaca -= 1;
        listSparepart[index] = Instantiate(kacaMobil, new Vector3(kacaMobil.transform.position.x + 42f * index, kacaMobil.transform.position.y, kacaMobil.transform.position.z), Quaternion.identity) as GameObject;
        listSparepart[index].transform.SetParent(canvas.transform, false);
        jumlahKacaMob += 1;
        index += 1;
    }

    public void simpanMesin()
    {
        jumlahBesi -= 1;
        listSparepart[index] = Instantiate(mesin, new Vector3(mesin.transform.position.x + 42f * index, mesin.transform.position.y, mesin.transform.position.z), Quaternion.identity) as GameObject;
        listSparepart[index].transform.SetParent(canvas.transform, false);
        jumlahMesin += 1;
        index += 1;
    }

    public void simpanBody()
    {
        jumlahPlastik -= 1;
        listSparepart[index] = Instantiate(body, new Vector3(body.transform.position.x + 42f * index, body.transform.position.y, body.transform.position.z), Quaternion.identity) as GameObject;
        listSparepart[index].transform.SetParent(canvas.transform, false);
        jumlahBody += 1;
        index += 1;
    }

    public void simpanKerangka()    
    {
        jumlahBesi -= 1;
        listSparepart[index] = Instantiate(kerangka, new Vector3(kerangka.transform.position.x + 42f * index, kerangka.transform.position.y, kerangka.transform.position.z), Quaternion.identity) as GameObject;
        listSparepart[index].transform.SetParent(canvas.transform, false);
        jumlahKerangka += 1;
        index += 1;
    }

    public void simpanBan()
    {
        jumlahKaret -= 1;
        listSparepart[index] = Instantiate(ban, new Vector3(ban.transform.position.x + 42f * index, ban.transform.position.y, ban.transform.position.z), Quaternion.identity) as GameObject;
        listSparepart[index].transform.SetParent(canvas.transform, false);
        jumlahBan += 1;
        index += 1;
    }

    public void simpanMobil()
    {
        
        jumlahBody -= 1;
        MoveMaterial("body(Clone)");
        jumlahKacaMob -= 1;
        MoveMaterial("kaca(Clone)");
        jumlahKerangka -= 1;
        MoveMaterial("kerangka(Clone)");
        jumlahMesin -= 1;
        MoveMaterial("mesin(Clone)");
        jumlahBan -= 1;
        MoveMaterial("ban(Clone)");
        listSparepart[index] = Instantiate(Mobil, new Vector3(Mobil.transform.position.x + 42f * index, Mobil.transform.position.y, Mobil.transform.position.z), Quaternion.identity) as GameObject;
        listSparepart[index].transform.SetParent(canvas.transform, false);
        jumlahMobil += 1;
        index += 1;
        Debug.Log(index);        
    }
    
    public void MoveMaterial(string objectName){
        int j = 0;
        int k = 0;
        bool notRemoved = true;
        while (j < index && notRemoved){
            if(listSparepart[j].name == objectName){
                notRemoved = false;
                Destroy(listSparepart[j]);
                for(k = j; k < index-1; k++){
                    listSparepart[k] = listSparepart[k+1];    
                }
                listSparepart[k+1] = null;
                index--;
                for (k = 0; k < index; k++){
                    listSparepart[k].GetComponent<RectTransform>().anchoredPosition = new Vector3(storageZero.GetComponent<RectTransform>().anchoredPosition.x + 42f * k, storageZero.GetComponent<RectTransform>().anchoredPosition.y);
                }
                
                return;
            }
            j++;
        }
    }

}