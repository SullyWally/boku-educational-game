﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorManager : MonoBehaviour
{
    [SerializeField] public Texture2D cursorTexture2D;
    public Texture2D cursorHover;

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    void Start()
    {
        Cursor.SetCursor(cursorTexture2D, new Vector2(10, 10), CursorMode.ForceSoftware);
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Cursor.SetCursor(cursorHover, new Vector2(10, 10), CursorMode.ForceSoftware);
        }

        if (Input.GetMouseButtonUp(0))
        {
            Cursor.SetCursor(cursorTexture2D, new Vector2(10, 10), CursorMode.ForceSoftware);
        }
    }
}
