﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelHandler : MonoBehaviour
{
    public GameObject[] Level;
    public Sprite unlocked;
    public Sprite locked;

    public GameObject warningText;

    GameObject userData;

    void Start()
    {
        warningText.SetActive(false);
        userData = GameObject.FindGameObjectWithTag("DataHandler");
        GetComponent<UserSelectScript>();

        for (int i = 0; i < Level.Length; i++)
        {
            Level[i].GetComponent<Image>().sprite = locked;
            Level[i].GetComponent<Button>().enabled = false;
            Level[i].transform.GetChild(0).gameObject.SetActive(false);
        }
    }

    void Update()
    {        
        for (int i = 0; i < UserSelectScript.levelCleared.Length; i++)
        {
            if(UserSelectScript.levelCleared[i] == true)
            {
                Level[i].GetComponent<Image>().sprite = unlocked;
                Level[i].transform.GetChild(0).gameObject.SetActive(true);
                Level[i].GetComponent<Button>().enabled = true;
            }
        }
    }

    public void SaveGame()
    {
        userData.GetComponent<UserSelectScript>().SaveGame();
        StartCoroutine(waitForSave());
    }

    IEnumerator waitForSave()
    {
        warningText.SetActive(true);
        warningText.GetComponent<Text>().text = "Sedang Menyimpan";
        yield return new WaitForSeconds(2f);
        warningText.GetComponent<Text>().text = "Berhasil Menyimpan";
    }
}
