﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupTextScript : MonoBehaviour
{
    public float DestroyTime = 3f;
    public Vector3 offset = new Vector3(0, 2, 0);
    public Vector3 RandomSizeIntensity = new Vector3(0.5f, 0, 0);
    void Start()
    {
        Destroy(this.gameObject, DestroyTime);
        transform.localPosition += offset;
        transform.localPosition += new Vector3(Random.Range(-RandomSizeIntensity.x, RandomSizeIntensity.x),
        Random.Range(-RandomSizeIntensity.y, RandomSizeIntensity.y),
        Random.Range(-RandomSizeIntensity.z, RandomSizeIntensity.z));
    }
}
