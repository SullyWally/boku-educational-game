﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RetrvDataScript : MonoBehaviour
{
    public GameProgress data;
    string jsonDeserialize;

    //DATA
    public static string username;
    public static int levelComplete;
    public static int star;

    public static bool[] levelCleared;
    public static int[] starAquired;
    public static int score;

    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }

    void Start()
    {
        levelCleared = new bool[10];
        starAquired = new int[10];
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void saveData()
    {
        data.levelComplete = levelComplete;
        data.star = star;
        data.levelCleared = levelCleared;
        data.starAquired = starAquired;
        
        string json = JsonUtility.ToJson(data);       
        Debug.Log(json);
    }

    public void LoadData()
    {
    }

    private void Script_ValueChanged(object sender, object e)
    {
        data = JsonUtility.FromJson<GameProgress>(jsonDeserialize);

        levelComplete = data.levelComplete;
        star = data.star;
        levelCleared = data.levelCleared;
        starAquired = data.starAquired;
    }
}
