﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UserSelectScript : MonoBehaviour
{
    string URL = "https://unityprojectboku.000webhostapp.com/";
    public string[] usersData;

    public Text text;

    public InputField email;
    public InputField password;

    public LoadingScript loading;
    public userUpdateScript updateData;

    public static string _username;
    public static string _email;
    public static string _password;
    public static string _progress;
    public static int _index;

    public GameProgress data;

    public static int levelComplete;
    public static int star;

    public static bool[] levelCleared;
    public static int[] starAquired;
    public static int score;

    [System.Obsolete]
    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        StartCoroutine(getData());
    }

    private void Start()
    {
        GetComponent<userUpdateScript>();
    }

    public IEnumerator getData()
    {
        WWW users = new WWW (URL);
        yield return users;
        string usersDataString = users.text;
        usersData = usersDataString.Split(';');

        Debug.Log(GetValueData(usersData[0], "email:"));  
    }

    public string GetValueData(string data, string index)
    {
        string value = data.Substring(data.IndexOf(index) + index.Length);
        if(value.Contains("|"))
        {
            value = value.Remove(value.IndexOf("|"));
        }
        return value;
    }

    public void login()
    {        
        string userValue;
        string passValue;

        if (email.text != "" && email.text != "")
        {
            for (int i = 0; i < usersData.Length - 1; i++)
            {
                userValue = GetValueData(usersData[i], "email:");
                passValue = GetValueData(usersData[i], "password:");
                if (email.text.ToString() == userValue && password.text.ToString() == passValue)
                {
                    Debug.Log("Login Succes : " + userValue + " : " + passValue);
                    text.GetComponent<Text>().text = "Login Succes";
                    _index = i;
                    _username = GetValueData(usersData[_index], "username:");
                    _email = GetValueData(usersData[_index], "email:");
                    _password = GetValueData(usersData[_index], "password:");
                    _progress = GetValueData(usersData[_index], "progress:");
                    loading.LoadScene();
                    return;
                }

                else if (i == usersData.Length - 2)
                {
                    text.GetComponent<Text>().text = "Email atau Password Salah";
                }              
            }
        }

        else
        {
            text.GetComponent<Text>().text = "Gagal!!";
        }              
    }

    public void LoadGame()
    {
        StartCoroutine(getData());

        _progress = GetValueData(usersData[_index], "progress:");
        data = JsonUtility.FromJson<GameProgress>(_progress);

        levelComplete = data.levelComplete;
        star = data.star;
        levelCleared = data.levelCleared;
        starAquired = data.starAquired;
        score = data.score;
        Debug.Log(usersData[_index]);
    }

    public void SaveGame()
    {
        data.levelComplete = levelComplete;
        data.star = star;
        data.levelCleared = levelCleared;
        data.starAquired = starAquired;
        data.score = score;

        _progress = JsonUtility.ToJson(data);
        Debug.Log(_progress);
        StartCoroutine(updateData.EditUser(_username, _email, _password, _progress, "username", _username));        
    }
}
