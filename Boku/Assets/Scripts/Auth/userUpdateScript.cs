﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class userUpdateScript : MonoBehaviour
{
    string URL = "https://unityprojectboku.000webhostapp.com/";

    public IEnumerator EditUser(string username, string email, string password, string progress, string wF, string wC)
    {
        WWWForm form = new WWWForm();
        form.AddField("editUsername", username);
        form.AddField("editEmail", email);
        form.AddField("editPassword", password);
        form.AddField("editProgress", progress);

        form.AddField("whereField", wF);
        form.AddField("whereCondition", wC);

        WWW www = new WWW(URL, form);

        yield return new WaitForSeconds(2f);
        StartCoroutine(this.gameObject.GetComponent<UserSelectScript>().getData());
        Debug.Log("SaveSucces");
    }
}
